#+TITLE: Fish - The Friendly Interactive Shell
#+DESCRIPTION: Knowledge Base - Fish
#+SETUPFILE: ~/nc/gitlab-repos/distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* History
** History Sessions
Command history for all sessions is stored in the file =~/.local/share/fish/fish_history=. This file stores each command and the timestamp of when it was entered.

Each session begins with the history from this file, but stores commands entered within the session in memory. This is why a session does not recall commands entered in other sessions after it was started. However, a session can add command history from all other sessions to its own by entering history merge. This is a one-time merge that can be manually repeated later. The histories of other sessions will not be continually merged into the current session.

** Run Command Without It Being Saved To History
All commands entered are saved in command history, except those that begin with a =space=. Those are treated like incognito commands. Also, duplicate commands are automatically removed, retaining only their most recent occurrence.

To navigate through all previously entered commands, press the up and down arrow keys. Press enter to execute the displayed command or press ctrl-c to exit without executing one. To restrict to commands that contain a given substring, type the substring before pressing the arrow keys.
To list all commands in the history, enter history.

** History Commands
To list only commands that contain a given substring, enter =history substring=. This is the same as entering =history search --contains substring (-c)=.

To list only commands that begin with a given prefix, enter =history search --prefix prefix (-p)=.

To include the date and time that commands were issued, add the =--show-time-prepends (-t)= switch.

To limit the number of commands output, add the =--max (-n)= switch followed by a number. For example, =history -n 5=. Note that the space after =-n= is required.

To clear all command history, enter =history clear=.

To delete commands from history that contain a given substring, enter =history delete --contains substring (-c)=.

To delete commands from history that begin with a given prefix, enter =history delete --prefix prefix (-p)=.

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
