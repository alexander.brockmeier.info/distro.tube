#+TITLE: Shuf
#+DESCRIPTION: Knowledge Base - Shuf
#+SETUPFILE: ~/nc/gitlab-repos/distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* shuf
Generate random permutations.

** Randomize the order of lines in a file and output the result:
#+begin_example
shuf filename
seq 10 | shuf
#+end_example

** Only output the first 5 entries of the result:
#+begin_example
shuf -n 5 filename
seq 10 | shuf -n 5
#+end_example

** Write the output to another file:
=NOTE:= the output file is the one immediately after the -o flag!
#+begin_example
shuf test.txt -o test2.txt
#+end_example

** Generate 3 random numbers in the range 1-10 (inclusive):
=NOTE:= The "repeat" flag means output lines can appear more than once.
#+begin_example
shuf -n 3 -i 1-10 --repeat
#+end_example

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
