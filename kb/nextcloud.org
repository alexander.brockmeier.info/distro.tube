#+TITLE: Nextcloud
#+DESCRIPTION: Knowledge Base - Nextcloud
#+SETUPFILE: ~/nc/gitlab-repos/distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* Installing Nextcloud on DigitalOcean Ubuntu Server
** Prerequisites
In order to complete the steps in this guide, you will need the following:
*** A DigitalOcean droplet with Ubuntu Server 20.04.  Basic plan is fine for me.
*** Be sure to update the server: =sudo apt update && sudo apt upgrade=
*** A non-root sudo-enabled user and firewall configured on your server.
+ Add a user on Ubuntu: =adduser name=
+ Add user to sudo group: =usermod -aG sudo name=
*** A domain name pointed to your server.
** Installing Nextcloud
*** sudo snap install nextcloud
The Nextcloud package will be downloaded and installed on your server.
*** snap changes nextcloud
To verify it's installed.
** Configure Administrative Account
*** sudo nextcloud.manual-install name password
This make a take a few minutes.  If successful you should get the output: Nextcloud was successfully installed
** Adjusting The Trusted Domains
*** sudo nextcloud.occ config:system:get trusted_domains
View the trusted_domains array.  At first, the only trusted domain is localhost.
*** sudo nextcloud.occ config:system:set trusted_domains 1 --value=example.com
+ Adds an entry for our domain.
+ To verify, again run: =sudo nextcloud.occ config:system:get trusted_domains=
** SSL and Let's Encrypt
*** sudo ufw allow 80,443/tcp
Opens the ports in the firewall that Let’s Encrypt uses to validate domain ownership.
*** sudo nextcloud.enable-https lets-encrypt
Requests a Let's Encrypt certificate.  You will be asked if your server meets the conditions necessary to request a certificate.  Answer "y".
*** Please enter an email address (for urgent notices or key recovery):
*** Please enter your domain name(s) (space-separated): example.com
** Login to Nextcloud
*** Go to https://example.com
Since you have already configure an administrator account from the command line, you will be taken to the Nextcloud login page.
*** After logging in...
The first time you enter, a window will be displayed with some introductory text and links to various Nextcloud clients that can be used to access your Nextcloud instance.
** Firewall
*** sudo apt install ufw
*** sudo vim /etc/default/ufw
Ubuntu server has IPv6 enabled, ensure that UFW is configured to support IPv6 so that it will manage firewall rules for IPv6 in addition to IPv4. Set: IPV6=yes
*** sudo ufw allow ssh   OR   sudo ufw allow 22
*** sudo ufw enable

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
