#+TITLE: Uniq
#+DESCRIPTION: Knowledge Base - uniq
#+SETUPFILE: ~/nc/gitlab-repos/distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* uniq
Output the unique lines from the given input or file. Since it does not detect repeated lines unless they are adjacent, we need to sort them first. While piping sort into uniq is seen as the correct way to do things, piping uniq into sort is usually seen as incorrect.

- Display each line once:
  sort .bashrc | uniq
  =NOTE:= fi only appears once even though it appears many times in the file.  Also, be aware that simply using "sort -u" would achieve the same thing.

- Display only unique lines:
  sort .bashrc | uniq -u
  =NOTE=: fi no longer is listed in the results. Only "unique" lines are returned.

- Display only duplicate lines:
  sort .bashrc | uniq -d
  =NOTE:= The opposite of the above command.

- Display number of occurrences of each line along with that line:
  sort .bashrc | uniq -c

- Display number of occurrences of each line, sorted by the most frequent:
  sort .bashrc | uniq -c | sort -nr

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
