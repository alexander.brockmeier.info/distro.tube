#+TITLE: Xargs
#+DESCRIPTION: Knowledge Base - xargs
#+SETUPFILE: ~/nc/gitlab-repos/distro.tube/org-html-themes/org/theme-readtheorg.setup
#+OPTIONS: num:nil ^:{}
#+INCLUDE: "~/nc/gitlab-repos/distro.tube/header.org"

* What is xargs?
Execute a command with piped arguments coming from another command, a file, etc. The input is treated as a single block of text and split into separate pieces on spaces, tabs, newlines and end-of-file.

The xargs command in UNIX is a command line utility for building an execution pipeline from standard input. Whilst tools like grep can accept standard input as a parameter, many other tools cannot. Using xargs allows tools like echo and rm and mkdir to accept standard input as arguments.

* Examples
** Run a command using the input data as arguments:
#+begin_example
arguments_source | xargs command
#+end_example

** Run multiple chained commands on the input data:
#+begin_example
arguments_source | xargs sh -c "command1 && command2 | command3"
#+end_example


** Take output from cat and create an argument list
#+begin_example
cat list | xargs
#+end_example

Wait!  I thought xargs took output from command and passed it into another command.  So what command is xargs using here?

=The -t flag=
#+begin_example
cat list | xargs -t
#+end_example
You will see that xargs uses echo since we didn't specify another command.

=The -n flag=
#+begin_example
cat list | xargs -t -n 1
#+end_example
It echos them one at a time now instead of a single echo.

** Generate a compact list of all Linux user accounts on the system
#+begin_example
cut -d: -f1 < /etc/passwd | sort | xargs
#+end_example


** ls example
#+begin_example
/bin/ls
/bin/ls | xargs ls
#+end_example

=the -I flag=
The braces symbolize all the input
#+begin_example
ls | xargs -I {} echo "/home/dt/{}"
#+end_example

The X's symbolize all the input
#+begin_example
ls | xargs -I XXXX echo "/home/dt/XXXX"
#+end_example

** Make 1000 sequential .txt files
#+begin_example
seq 1000 | xargs -I {} touch {}.txt
#+end_example

=convoluted example=
#+begin_example
/bin/ls | cut -d. -f1 | grep -v list | xargs -I {} mv {}.txt {}.text
#+end_example

** Mkdir
#+begin_example
echo 'one two three' | xargs mkdir
ls
one two three
#+end_example

When filenames contains spaces you need to use -d option to change delimiter
touch 'test file'
#+begin_example
/bin/ls | xargs -n 1
#+end_example
test and file are on their own lines...not good!

#+begin_example
/bin/ls | xargs -n 1 -d \n
#+end_example
Sets new lines as delimiter and now output is good.

** Find
Delete all files with a `.backup` extension (`-print0` uses a null character to split file names, and `-0` uses it as delimiter):
#+begin_example
find . -name '*.backup' -print0 | xargs -0 rm -v
#+end_example
Rm files older than two weeks in the /tmp folder
#+begin_example
find /tmp -mtime +14 | xargs rm
#+end_example
xargs vs exec
The find command supports the -exec option that allows arbitrary commands to be performed on found files. The following are equivalent.

#+begin_example
find foo -type f -name "*.txt" -exec rm {} \;
find foo -type f -name "*.txt" | xargs rm
#+end_example

So which one is faster? Let’s compare a folder with 1000 files in it.

#+begin_example
time find . -type f -name "*.txt" -exec rm {} \;
0.35s user 0.11s system 99% cpu 0.467 total

time find . -type f -name "*.txt" | xargs rm
0.00s user 0.01s system 75% cpu 0.016 total
#+end_example

Clearly using xargs is far more efficient. In fact several benchmarks suggest using xargs over exec {} is six times more efficient.

Parallel runs of up to `max-procs` processes at a time; the default is 1. If `max-procs` is 0, xargs will run as many processes as possible at a time:
#+begin_example
arguments_source | xargs -P max-procs command
seq 5 | xargs -n 1 -P 1 bash -c 'echo $0; sleep 1'
#+end_example

#+INCLUDE: "~/nc/gitlab-repos/distro.tube/footer.org"
