# distro.tube

This is the source code for my Gemini capsule which is located at gemini://distro.tube as well as my website located at https://distrotube.com

# Font

You can find the ascii font I used for my sites [here](https://patorjk.com/software/taag/#p=display&f=Cricket&t=dt%20ARTICLES).

# Submission Guidelines

Have an idea for an article? Want to improve the CSS? Does this README need to be fixed? Send in a merge request and I will try to review it and implement it.

Please follow a few basic guidelines when submitting merge requests:
+ When writting articles, please write them as gmi files as opposed to html, I use a script to convert the articles.
+ Ensure that what you are submitting is stylistically consistent and works.
+ Please be mindful of fish users when contributing scripts. Scripts should have /usr/bin/env bash as the shebang as opposed to /bin/sh.
