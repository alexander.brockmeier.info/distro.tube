```
                          __       _______ _______ _______ ___ _______ ___     _______ _______
 .-----.--.--.-----.-----|  |_    |   _   |   _   |       |   |   _   |   |   |   _   |   _   |
 |  _  |  |  |  -__|__ --|   _|   |.  1   |.  l   |.|   | |.  |.  1___|.  |   |.  1___|   1___|
 |___  |_____|_____|_____|____|   |.  _   |.  _   `-|.  |-|.  |.  |___|.  |___|.  __)_|____   |
 |_____|                          |:  |   |:  |   | |:  | |:  |:  1   |:  1   |:  1   |:  1   |
                                  |::.|:. |::.|:. | |::.| |::.|::.. . |::.. . |::.. . |::.. . |
                                  `--- ---`--- ---' `---' `---`-------`-------`-------`-------'
```

# Using dwmblocks With dwm's Bar
By Ronnie Nissan at April 20, 2020

## Introduction
Dwmblocks is one of the many status bars available at suckless.org's status monitor page (link at end of article), but I see it as the best one available, as it is easy to configure and it uses a blocks concept just like i3blocks, so you can write simple commands that print something (like volume) to the stdout and feed it into dwmblocks, give it an update interval, a signal, and it will print it to the WM_NAME which is the name of the root window of x11 and is managed by dwm. Here I will show you how to install, configure and run it. I'll also show you howto write a simple script to print your the date to the status bar with an icon next to it

## Installation
To install dwmblocks, you'll first have to clone the repository from GitHub

```
https://github.com/torrinfail/dwmblocks.git #then cd into the dwmblocks
cd dwmblocks
```

Then you install it the same way you install any other suckless tool, by compiling the source code and then installing it, using the following commands:

```
make
sudo make install
```

This will install dwmblocks to your system but you still can't use it as it is.

## Making The Script
We will make a simple script that will print the date to the status bar. In the following example we use the date command and printf to print a formatted string and show it in dwm's bar.

```
#!/bin/sh
date=$(date '+%b %d (%a) %H:%M')
icon='icon'
printf " %s %s \\n" "$icon" "$date"
```

In the above script we made a variable called date, which runs the shell command of date with the specific formatting, you can change the formatting by reading the man page for date. On the third line we set another optional variable called icon, which sets a simple icon, it can be an emoji, or an icon from font awesome for example. On line 4 of the script we call the printf function which prints out a formatted string to the stdout, and here it will print the icon of your choice, then the date as the format you specified in line 2. And the results of the above will be:

```
icon Apr 14 (Tue) 02:41
```

## Configuration
Now that you have working script that gives you the current time, you should save it to a file called “time.sh” for example and put it in your path, along with any other script you want to use with dwmblocks. then we go back to the dwmblocks directory and we edit the blocks.h file.

```
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{"",         "time.sh",	     60,                     0},
	{"",         "brightness.sh",	 0,                      11},
};
```

Above we add our script “time.sh” under command, set the update interval to 60 seconds and the update signal to 0, we leave the icon option empty as we already added an icon though out script. We assume we have another script called brightness.sh which prints the brightness to the status bar, but we want it to update only when we make a change to the screen brightness and that's why we have the update interval to 0 and the update signal to 11. After you finish adding your scripts (blocks) to the config.h, you must recompile and install dwmblocks again.

So now if we run pkill -RTMIN+11 dwmblocks from a terminal, the brightness block will be updated, so you should assign a different signal to each command that you want to update only when a change happens (volume for example) and call the above command every time you change brightness or volume, change the -RTMIN+11 to the signal you put in your blocks.h for that specific command.

For example, I use sxhkd to set a hotkey that uses “light” to change the brightness. so I have the following in my sxhkdrc

```
super + Up
    light -A 5 && pkill -RTMIN+11 dwmblocks
super + Down
    light -U 5 && pkill -RTMIN+11 dwmblocks
```

There might be better ways to do this, but this is the one I use.

After you finish setting up dwmblocks the way you want with your blocks, you should autostart dwmblocks though .xinitrc or add it to your autostart file if you are using the autostart patch for dwm (link at end of article).

## Backup
First of all you have to be in the directory of the program you want to make a backup of. then you'll rename the remote of the git repository from origin to upstream.

```
git remote rename origin upstream
```

Then you'll go to GitHub.com (or Gitlab) and make a new repository, call it the same as the program you are backing up for convenience. then copy the repository's URL and type:

```
git add remote origin <the repository's url>
```

Now if you type:

```
git remote -v
```

You'll see that you have two remotes called upstream (pointing to suckless.org) and one called origin (pointing to your repository). now all that's left is that you push your build to the master branch of your repository, to do that you type:

```
git push origin master
```

If the dwmblocks developer updated the program, all you'll have to do is, go to that programs directory, and run the following:

```
git pull upstream master
```

For more scripts, check my dotfiles (link below) repository on my Github page and check the “statusbar” directory inside my Script directory.

## Links
=> https://dwm.suckless.org/status_monitor/ Dwm Status Monitor
=> https://dwm.suckless.org/patches/autostart/ Dwm Autostart Patch
=> https://github.com/ronniedroid/.dotfiles My Dotfiles
